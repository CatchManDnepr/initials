

class Initails{
    
    /*
    function for check element on letters from alphabet
    @param element
    @return true/false
    */
    public char upcase(char el){
	if (el > 96 && el < 123){
	    return (char)((int)el - 32);
	}
	return el;
    }

    /*
    function for get initails
    @param text
    @return initails
    */
    public String getInitails(String []text){
	String initails = "";
	for(int i = 0; i < text.length; i++){
	    initails += upcase(text[i].charAt(0));
	}
	return initails;
    }
}